import { Injectable } from '@angular/core';
import { Video } from "../interface/video";

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  video: Array<Video> = [
    {
      image: '',
      type: '',
      title: '',
      date: '',
      nationality: '',
      author: '',
      actor: '',
      synopsis: '',
      quality: ''
    }
  ];
  constructor() { }

}
