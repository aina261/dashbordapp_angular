import { Component, OnInit } from '@angular/core';
import { Video } from "../../interface/video";
import { VideoService } from "../../service/video.service";

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {

  videos : Array<Video> = [];

  constructor() { }



  ngOnInit() {
  }

}
