export interface Photo {
  image: String;
  title: String;
  description: String;
}
