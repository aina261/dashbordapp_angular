export interface Video {
  image: String;
  type: String;
  title: String;
  date: String;
  nationality: String;
  author: String;
  actor: String;
  synopsis: String;
  quality: String;
}
