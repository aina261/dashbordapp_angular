export interface Music {
  image: String;
  type: String;
  title: String;
  author: String;
  quality: String;
  lenght: String;
}
