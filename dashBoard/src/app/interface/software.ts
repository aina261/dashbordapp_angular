export interface Software {
  image: String;
  title: String;
  description: String;
}
