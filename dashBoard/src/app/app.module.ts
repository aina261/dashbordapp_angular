import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { PhotoComponent } from './main/photo/photo.component';
import { MusicComponent } from './main/music/music.component';
import { VideoComponent } from './main/video/video.component';
import { SoftwareComponent } from './main/software/software.component';
import { OtherComponent } from './main/other/other.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    PhotoComponent,
    MusicComponent,
    VideoComponent,
    SoftwareComponent,
    OtherComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
